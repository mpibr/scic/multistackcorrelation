/*
 * MultistackCorrelation macro
 *     calculate correlation to a reference channel
 *     use all reference rotations
 *     coloc2 - Pearson's R value
 * version 0.1
 * Mar 2022
 * sciclist@brain.mpg.de
 */


/* check pathname for trailing separator
 * @param: path of interest
 */
function checkPathSeparator(path)
{
    if (!endsWith(path, File.separator))
        path = path + File.separator;
    return path;
}


 /* get all files from direcotry
  * @param: input directory
  * @param: file extension
  */
 function getFileListWithExtension(path, extension)
 {
    listOfFiles = getFileList(path);
    listOfExtension = newArray;
    i = 0;
    for (f = 0; f < listOfFiles.length; f++) {
        fileName = listOfFiles[f];
        if (endsWith(toLowerCase(fileName), extension)) {
            fullFileName = path + fileName;
            listOfExtension = Array.concat(listOfExtension, fullFileName);
            i++;
        }
    }
    return listOfExtension;
}


/* get all files from direcotry
  * @param: output directory
  * @param: list of files
  */
function preprocessStacks(path, files)
{
    countSlices = 0;
    for (f = 0; f < files.length; f++) {
        showProgress(f, files.length);
        fileIn = files[f];
        fileName = File.getName(fileIn);
        
        run("Bio-Formats Windowless Importer", "open="+fileIn);
        countSlices = nSlices;
        convertSignedToUnsigned16bit();
        exportStackToImages(path);
        //break;
    }
    return countSlices;
}


/* get titles of all open windows
 */
function collectTitles()
{
    nOpenWindows = nImages;
    titles = newArray;
    for (i = 1; i <= nOpenWindows; i++) {
        selectImage(i);
        openTitle = getTitle();
        titles = Array.concat(titles, openTitle);
    }
    return titles;
}


/* close all active windows
 */
function closeAll()
{
    while (nImages > 0) close();
}


/* correlateChannels
 * @param: titles of open windows
 * @param: reference channel
 */
function correlateChannels(openTitles, refChannel)
{
    for (c = 0; c < openTitles.length; c++) {

        if (c == refChannel)
            continue;

        run("Coloc 2", "channel_1=" +
        openTitles[refChannel] +
        " channel_2=" +
        openTitles[c] +
        " roi_or_mask=<None>" +
        " threshold_regression=Costes spearman's_rank_correlation" +
        " costes'_significance_test" +
        " psf=3 costes_randomisations=20");

    }
}


/* stack correlation
 * @param: file name of input image
 * @param: reference channel
 */
function processStackCorrelation(fileIn, refChannel)
{
    run("Bio-Formats Windowless Importer", "open="+fileIn);
    countSlices = nSlices;
    run("Stack to Images");
    openTitles = collectTitles();
    correlateChannels(openTitles, refChannel);
    
    print(nResults);

    closeAll();
}


/* main macro
 */
macro "MultistackCorrelation"
{
    // set defaults
    fileTypes = newArray(".czi", ".tif");
    refChannel = 2;

    // create input dialog
    Dialog.create("Macro Arguments");
    Dialog.addChoice("image type", fileTypes, fileTypes[0]);
    Dialog.addNumber("reference channel", refChannel);
    Dialog.show();

    // parse arguments
    fileExtension = Dialog.getChoice();
    refChannel = Dialog.getNumber() - 1; // convert to array index
    
    // set directories
    pathInput = getDirectory("choose a directory");

    // grab all files for stitching
    filesList = getFileListWithExtension(pathInput, fileExtension);

    // pipeline
    setBatchMode(true);
    for (f = 0; f < filesList.length; f++) {
        processStackCorrelation(filesList[f], refChannel);
        break;
    }

    setBatchMode(false);
}